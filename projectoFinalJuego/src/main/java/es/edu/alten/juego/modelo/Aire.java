package es.edu.alten.juego.modelo;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Qualifier;

@ManagedBean
@Qualifier("aire")
public class Aire extends PiedraPapelTieraFactory {
	public Aire() {
		this("aire", AIRE);
	}

	
	public Aire(String pNom, int pNum) {
		super(pNom, pNum);
	}


	@Override
	public boolean isMe(int pNum) {
		return pNum==AIRE;
	}

	@Override
	public int comparar(PiedraPapelTieraFactory pPiedPapelTijera) {
		// TODO Completar 
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {		
		
		case FUEGO:
		case PIEDRA:
		case PISTOLA:
		case LINTERNA:
		case DIABLO:
		case DRAGON:
		case AGUA:	
			resul=1;
			this.descripcionREsultado = "aire le gana a " + pPiedPapelTijera.getNombre();
			break;
		case SERPIENTE:
		case PAPEL:
		case ESPONJA:
		case LOBO:
		case ARBOL:
		case HUMANO:
		case TIJERA:
			resul=-1;
			this.descripcionREsultado = "aire perdi� con " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul=0;
			this.descripcionREsultado = "aire empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}

}
