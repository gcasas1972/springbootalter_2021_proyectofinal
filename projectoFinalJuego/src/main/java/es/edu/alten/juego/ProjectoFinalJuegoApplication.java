package es.edu.alten.juego;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectoFinalJuegoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectoFinalJuegoApplication.class, args);
	}

}
