package es.edu.alten.juego.modelo;

import java.util.ArrayList;
import java.util.List;

public abstract class PiedraPapelTieraFactory {
	public final static int PIEDRA 	= 1;
	public final static int PAPEL 	= 2;
	public final static int TIJERA 	= 3;
	
	public final static int PISTOLA	= 4; //Erika
	public final static int LINTERNA= 5; //Erika
	
	//entra en conflicto con pistola y linterna que tenian los mismos valores
	public final static int LAGARTO = 16;
	public final static int SPOCK	= 17;
	
	//se deberan quitar lagarto y spock y continuar a partir de tijera
	
	
	public final static int DIABLO	= 6; //Alba	
	public final static int DRAGON	= 7; //Alba	
	
	public final static int AGUA 	= 8; //Ivan
	public final static int AIRE	= 9; //Ivan
	
	public final static int ESPONJA	= 10; //Yeli
	public final static int LOBO	= 11; //Yeli	
	
	public final static int ARBOL	= 12; //Victor	
	public final static int HUMANO	= 13; //Victor
	
	public final static int SERPIENTE= 14; //Antonio
	public final static int FUEGO	= 15; //Antonio
	
	
	//atributos
	protected String 								descripcionREsultado;
	private static List<PiedraPapelTieraFactory> 	elementos			;
	protected String 								nombre				;
	protected int 									numero				;

	
	private static PiedraPapelTieraFactory piedra = new Piedra()  ;
	private static PiedraPapelTieraFactory papel = new Papel() ;
	private static PiedraPapelTieraFactory tijera = new Tijera() ;
	private static PiedraPapelTieraFactory pistola = new Pistola() ;
	private static PiedraPapelTieraFactory linterna = new Linterna() ;
	private static PiedraPapelTieraFactory diablo = new Diablo() ;
	private static PiedraPapelTieraFactory dragon = new Dragon() ;
	private static PiedraPapelTieraFactory agua = new Agua() ;
	private static PiedraPapelTieraFactory aire = new Aire() ;
	private static PiedraPapelTieraFactory esponja = new Esponja() ;
	private static PiedraPapelTieraFactory lobo = new Lobo() ;
	private static PiedraPapelTieraFactory arbol = new Arbol() ;
	private static PiedraPapelTieraFactory humano = new Humano() ;
	private static PiedraPapelTieraFactory serpiente = new Serpiente() ;
	private static PiedraPapelTieraFactory fuego = new Fuego() ;
	private static PiedraPapelTieraFactory lagarto = new Lagarto() ;
	private static PiedraPapelTieraFactory spock = new Spock() ;
	
	//constructores
	public PiedraPapelTieraFactory(String pNom, int pNum) {
		nombre = pNom;
		numero = pNum;
	}
	//gettter y setter

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getDescripcionREsultado() {
		return descripcionREsultado;
	}
	
	//metodo de negocio
	public abstract boolean isMe(int pNum);
	public abstract int comparar(PiedraPapelTieraFactory pPiedPapelTijera);
	
	public static PiedraPapelTieraFactory getInstance(int pNumero) {

		//el corazon del factory
		//1ro el padre reconoce a todos sus hijos
		elementos = new ArrayList<PiedraPapelTieraFactory>();
		
		elementos.add(piedra);
		elementos.add(papel);
		elementos.add(tijera);
		elementos.add(pistola);
		elementos.add(linterna);
		elementos.add(diablo);
		elementos.add(dragon);
		elementos.add(agua);
		elementos.add(aire);
		elementos.add(esponja);
		elementos.add(lobo);
		elementos.add(arbol);
		elementos.add(humano);
		elementos.add(serpiente);
		elementos.add(fuego);
		//tengo que agregar los nuevos 
		elementos.add(lagarto);
		elementos.add(spock);
		
		
		
		// es todo el codigo va a ser siemrpe el misom
		for (PiedraPapelTieraFactory piedraPapelTieraFactory : elementos) {
			if(piedraPapelTieraFactory.isMe(pNumero))
				return piedraPapelTieraFactory;
			 	
		}
		
				
		return null;
	}
	
	
}
