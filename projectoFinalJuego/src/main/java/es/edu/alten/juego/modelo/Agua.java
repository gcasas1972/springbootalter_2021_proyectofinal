package es.edu.alten.juego.modelo;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Qualifier;

@ManagedBean
@Qualifier("agua")
public class Agua extends PiedraPapelTieraFactory {
	public Agua() {
		this("agua", AGUA);
	}

	
	public Agua(String pNom, int pNum) {
		super(pNom, pNum);
	}


	@Override
	public boolean isMe(int pNum) {
		return pNum==AGUA;
	}

	@Override
	public int comparar(PiedraPapelTieraFactory pPiedPapelTijera) {
		// TODO Completar 
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {		
		case TIJERA:
		case FUEGO:
		case PIEDRA:
		case PISTOLA:
		case LINTERNA:
		case DIABLO:
		case DRAGON:
			
			resul=1;
			this.descripcionREsultado = "agua le gana a " + pPiedPapelTijera.getNombre();
			break;
		case AIRE:
		case PAPEL:
		case ESPONJA:
		case LOBO:
		case ARBOL:
		case HUMANO:
		case SERPIENTE:
			resul=-1;
			this.descripcionREsultado = "agua perdi� con " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul=0;
			this.descripcionREsultado = "agua empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}

}
