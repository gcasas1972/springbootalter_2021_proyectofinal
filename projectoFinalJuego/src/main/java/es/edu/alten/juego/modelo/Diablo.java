package es.edu.alten.juego.modelo;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Qualifier;

@ManagedBean
@Qualifier("diablo")

public class Diablo extends PiedraPapelTieraFactory{
	public Diablo() {
		this("diablo", DIABLO);
	}

	
	public Diablo(String pNom, int pNum) {
		super(pNom, pNum);
	}


	@Override
	public boolean isMe(int pNum) {
		return pNum==DIABLO;
	}

	@Override
	public int comparar(PiedraPapelTieraFactory pPiedPapelTijera) {
		// TODO Completar 
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {		
		case PISTOLA:
		case LINTERNA:
		case HUMANO:
		case SERPIENTE:
		case TIJERA:
		case FUEGO:
		case PIEDRA:
			resul=1;
			this.descripcionREsultado = "diablo le gana a " + pPiedPapelTijera.getNombre();
			break;
			
        case AIRE:
        case AGUA:
        case ESPONJA:
        case LOBO:
        case ARBOL:
        case PAPEL:
        case DRAGON:
			resul=-1;
			this.descripcionREsultado = "diablo perdi� con " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul=0;
			this.descripcionREsultado = "diablo empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}

}
