package es.edu.alten.juego.modelo;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Qualifier;

@ManagedBean
@Qualifier("serpiente")
public class Serpiente extends PiedraPapelTieraFactory {
	public Serpiente() {
		this("serpiente", SERPIENTE);
	}

	
	public Serpiente(String pNom, int pNum) {
		super(pNom, pNum);
	}


	@Override
	public boolean isMe(int pNum) {
		return pNum==SERPIENTE;
	}

	@Override
	public int comparar(PiedraPapelTieraFactory pPiedPapelTijera) {
		// TODO Completar 
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {		
		case AGUA:
		case AIRE:
		case PAPEL:
		case ESPONJA:
		case LOBO:
		case ARBOL:
		case HUMANO:	
			resul=1;
			this.descripcionREsultado = "serpiente le gana a " + pPiedPapelTijera.getNombre();
			break;
			
        case TIJERA:
        case PIEDRA:
        case PISTOLA:
        case LINTERNA:
        case DIABLO:
        case DRAGON:
        case FUEGO:
			resul=-1;
			this.descripcionREsultado = "serpiente perdi� con " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul=0;
			this.descripcionREsultado = "serpiente empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}

}
