package es.edu.alten.juego.modelo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Clase linterna
 * @author erika.castro
 *
 */
@Component
@Qualifier("linterna")
public class Linterna extends PiedraPapelTieraFactory {

	/**
	 * Constructor de clase
	 */
	public Linterna() {
		this("linterna", LINTERNA);
	}

	/**
	 * Contrustor de clase
	 * @param pNom Nombre
	 * @param pNum Numero
	 */
	public Linterna(String pNom, int pNum) {
		super(pNom, pNum);
	}

	/**
	 * Método que comprueba si el número obtenido es el esperado
	 */
	@Override
	public boolean isMe(int pNum) {
		return pNum == LINTERNA;
	}

	/**
	 * Realiza una comparación con todos los objetos y obtiene contra quien gana y contra quien pierde
	 */
	@Override
	public int comparar(PiedraPapelTieraFactory pPiedPapelTijera) {

		int resul = 0;

		switch (pPiedPapelTijera.getNumero()) {
		case FUEGO:
		case TIJERA:
		case SERPIENTE:
		case HUMANO:
		case ARBOL:
		case PISTOLA:
		case PIEDRA:
			resul = 1;
			this.descripcionREsultado = "linterna le gana a " + pPiedPapelTijera.getNombre();
			break;

		case ESPONJA:
		case PAPEL:
		case AIRE:
		case AGUA:
		case DRAGON:
		case DIABLO:
		case LOBO:
			resul = -1;
			this.descripcionREsultado = "linterna pierde con " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul = 0;
			this.descripcionREsultado = "linterna empata con " + pPiedPapelTijera.getNombre();

		}
		return resul;
	}

}
