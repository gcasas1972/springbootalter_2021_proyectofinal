package es.edu.alten.juego.modelo;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Qualifier;

@ManagedBean
@Qualifier("humano")
public class Humano extends PiedraPapelTieraFactory {

	public Humano() {
		this("humano", HUMANO);
	}

	
	public Humano(String pNom, int pNum) {
		super(pNom, pNum);
	}


	@Override
	public boolean isMe(int pNum) {
		return pNum==HUMANO;
	}

	@Override
	public int comparar(PiedraPapelTieraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {		
		
		case DRAGON:
		case AGUA:
		case AIRE:
		case PAPEL:
		case ESPONJA:
		case LOBO:
		case ARBOL:
			resul=1;
			this.descripcionREsultado = "humano le gana a " + pPiedPapelTijera.getNombre();
			break;
		case DIABLO:
		case LINTERNA:
		case PISTOLA:
		case PIEDRA:
		case FUEGO:
		case TIJERA:
		case SERPIENTE:
			resul=-1;
			this.descripcionREsultado = "humano perdi� con " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul=0;
			this.descripcionREsultado = "humano empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}
}

