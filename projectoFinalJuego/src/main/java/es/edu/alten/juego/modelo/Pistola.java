package es.edu.alten.juego.modelo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Clase pistola
 * @author erika.castro
 *
 */
@Component
@Qualifier("pistola")
public class Pistola extends PiedraPapelTieraFactory {

	/**
	 * Constructor de clase
	 */
	public Pistola() {
		this("pistola", PISTOLA);
	}

	/**
	 * Constructor de clase
	 * @param pNom nombre
	 * @param pNum numero
	 */
	public Pistola(String pNom, int pNum) {
		super(pNom, pNum);
	}

	/**
	 * Método que comprueba si el número obtenido es el esperado
	 */
	@Override
	public boolean isMe(int pNum) {
		return pNum == PISTOLA;
	}

	/**
	 * Realiza una comparación con todos los objetos y obtiene contra quien gana y contra quien pierde
	 */
	@Override
	public int comparar(PiedraPapelTieraFactory pPiedPapelTijera) {

		int resul = 0;

		switch (pPiedPapelTijera.getNumero()) {
		case PIEDRA:
		case TIJERA:
		case FUEGO:
		case SERPIENTE:
		case HUMANO:
		case LOBO:
		case ARBOL:
			resul = 1;
			this.descripcionREsultado = "pistola le gana a " + pPiedPapelTijera.getNombre();
			break;

		case ESPONJA:
		case PAPEL:
		case AIRE:
		case AGUA:
		case DRAGON:
		case DIABLO:
		case LINTERNA:
			resul = -1;
			this.descripcionREsultado = "pistola pierde con " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul = 0;
			this.descripcionREsultado = "pistola empata con " + pPiedPapelTijera.getNombre();

		}
		return resul;
	}

}
