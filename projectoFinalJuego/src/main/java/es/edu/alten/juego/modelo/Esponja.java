package es.edu.alten.juego.modelo;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Qualifier;

@ManagedBean
@Qualifier("esponja")
public class Esponja extends PiedraPapelTieraFactory {
	
	public Esponja() {
		this("esponja", ESPONJA);
	}
	public Esponja(String pNom, int pNum) {
		super(pNom,pNum);
		
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum==ESPONJA;
	}

	@Override
	public int comparar(PiedraPapelTieraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		//le gana 
		case PISTOLA:
		case LINTERNA:	
		case DIABLO:	
		case DRAGON:
		case AGUA:
		case AIRE:
		case PAPEL:
			resul=1;
			this.descripcionREsultado ="Esponja le gana a " + pPiedPapelTijera.getNombre();
			break ;
     //pierde con
		case PIEDRA:
		case FUEGO:	
		case TIJERA:	
		case SERPIENTE:
		case HUMANO:
		case ARBOL:
		case LOBO:
			resul=-1;
			this.descripcionREsultado ="Esponja perdió con " + pPiedPapelTijera.getNombre();
			break;
		default:			
			resul=0;
			this.descripcionREsultado = "Esponja empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}
}
