package es.edu.alten.juego.modelo;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Qualifier;

@ManagedBean
@Qualifier("lobo")
public class Lobo extends PiedraPapelTieraFactory {
	public Lobo() {
		this("lobo", LOBO);
	}
	public Lobo(String pNom, int pNum) {
		super(pNom,pNum);
		
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum==LOBO;
	}

	@Override
	public int comparar(PiedraPapelTieraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		//le gana 
		case LINTERNA:
		case DIABLO:	
		case DRAGON:	
		case AGUA:
		case AIRE:
		case PAPEL:
		case ESPONJA:	
			resul=1;
			this.descripcionREsultado ="Lobo le gana a " + pPiedPapelTijera.getNombre();
			break ;
     //pierde con
		case PISTOLA:
		case PIEDRA:	
		case FUEGO:	
		case TIJERA:
		case SERPIENTE:
		case HUMANO:
		case ARBOL:	
			resul=-1;
			this.descripcionREsultado ="Lobo perdió con " + pPiedPapelTijera.getNombre();
			break;
		default:			
			resul=0;
			this.descripcionREsultado = "Lobo empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}
}
