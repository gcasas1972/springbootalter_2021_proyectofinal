package es.edu.alten.juego.service;

import org.springframework.stereotype.Service;

import es.edu.alten.juego.modelo.PiedraPapelTieraFactory;

@Service
public class JuegoService {

	public PiedraPapelTieraFactory getIngence(int pValor) {
		return PiedraPapelTieraFactory.getInstance(pValor);
	}
}
