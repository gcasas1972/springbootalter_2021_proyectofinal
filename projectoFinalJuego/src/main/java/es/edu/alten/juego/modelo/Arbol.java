package es.edu.alten.juego.modelo;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Qualifier;

@ManagedBean
@Qualifier("arbol")
public class Arbol extends PiedraPapelTieraFactory {

	public Arbol() {
		this("arbol", ARBOL);
	}

	
	public Arbol(String pNom, int pNum) {
		super(pNom, pNum);
	}


	@Override
	public boolean isMe(int pNum) {
		return pNum==ARBOL;
	}

	@Override
	public int comparar(PiedraPapelTieraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {		
		
		case DIABLO:
		case DRAGON:
		case AGUA:
		case AIRE:
		case PAPEL:
		case ESPONJA:
		case LOBO:	
			resul=1;
			this.descripcionREsultado = "arbol le gana a " + pPiedPapelTijera.getNombre();
			break;
		case LINTERNA:
		case PISTOLA:
		case PIEDRA:
		case FUEGO:
		case TIJERA:
		case SERPIENTE:
		case HUMANO:
			resul=-1;
			this.descripcionREsultado = "arbol perdi� con " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul=0;
			this.descripcionREsultado = "arbol empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}
}
