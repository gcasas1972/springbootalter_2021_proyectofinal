package es.edu.alten.juego.modelo;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Qualifier;

@ManagedBean
@Qualifier("fuego")
public class Fuego extends PiedraPapelTieraFactory {
	public Fuego() {
		this("fuego", FUEGO);
	}

	
	public Fuego(String pNom, int pNum) {
		super(pNom, pNum);
	}


	@Override
	public boolean isMe(int pNum) {
		return pNum==FUEGO;
	}

	@Override
	public int comparar(PiedraPapelTieraFactory pPiedPapelTijera) {
		// TODO Completar 
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {		
		case PAPEL:
		case ESPONJA:
		case LOBO:
		case ARBOL:
		case HUMANO:
		case SERPIENTE:
		case TIJERA:
			resul=1;
			this.descripcionREsultado = "fuego le gana a " + pPiedPapelTijera.getNombre();
			break;
			
        case PIEDRA:
        case PISTOLA:
        case LINTERNA:
        case DIABLO:
        case DRAGON:
        case AGUA:
        case AIRE:
			resul=-1;
			this.descripcionREsultado = "fuego perdi� con " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul=0;
			this.descripcionREsultado = "fuego empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}

}
