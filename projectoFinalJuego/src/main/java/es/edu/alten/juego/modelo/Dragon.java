package es.edu.alten.juego.modelo;

import javax.annotation.ManagedBean;

import org.springframework.beans.factory.annotation.Qualifier;

@ManagedBean
@Qualifier("dragon")
public class Dragon extends PiedraPapelTieraFactory{
	public Dragon() {
		this("dragon", DRAGON);
	}

	
	public Dragon(String pNom, int pNum) {
		super(pNom, pNum);
	}


	@Override
	public boolean isMe(int pNum) {
		return pNum==DRAGON;
	}

	@Override
	public int comparar(PiedraPapelTieraFactory pPiedPapelTijera) {
		// TODO Completar 
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {		
		case PISTOLA:
		case LINTERNA:
		case TIJERA:
		case SERPIENTE:
		case PIEDRA:
		case FUEGO:
		case DIABLO:
			resul=1;
			this.descripcionREsultado = "dragon le gana a " + pPiedPapelTijera.getNombre();
			break;
			
        case AIRE:
        case AGUA:
        case ESPONJA:
        case LOBO:
        case HUMANO:
        case ARBOL:
        case PAPEL:
			resul=-1;
			this.descripcionREsultado = "dragon perdi� con " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul=0;
			this.descripcionREsultado = "dragon empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}

}
