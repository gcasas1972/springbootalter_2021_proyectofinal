package es.edu.alten.juego.modelo.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import es.edu.alten.juego.modelo.PiedraPapelTieraFactory;
import es.edu.alten.juego.service.JuegoService;

@SpringBootTest
public class DiabloDragonAlbaTest {

	@Autowired
	JuegoService juegoService;

	@Autowired
	@Qualifier("piedra")
	PiedraPapelTieraFactory piedra;

	@Autowired
	@Qualifier("papel")
	PiedraPapelTieraFactory papel;

	@Autowired
	@Qualifier("tijera")
	PiedraPapelTieraFactory tijera;

	@Autowired
	@Qualifier("lagarto")
	PiedraPapelTieraFactory lagarto;

	@Autowired
	@Qualifier("spock")
	PiedraPapelTieraFactory spock;
		
	@Autowired
	@Qualifier("pistola")
	PiedraPapelTieraFactory pistola;
	
	@Autowired
	@Qualifier("fuego")
	PiedraPapelTieraFactory fuego;
	
	@Autowired
	@Qualifier("serpiente")
	PiedraPapelTieraFactory serpiente;
	
	@Autowired
	@Qualifier("humano")
	PiedraPapelTieraFactory humano;
	
	@Autowired
	@Qualifier("arbol")
	PiedraPapelTieraFactory arbol;
	
	@Autowired
	@Qualifier("lobo")
	PiedraPapelTieraFactory lobo;
	
	@Autowired
	@Qualifier("esponja")
	PiedraPapelTieraFactory esponja;
	
	@Autowired
	@Qualifier("aire")
	PiedraPapelTieraFactory aire;
	
	@Autowired
	@Qualifier("agua")
	PiedraPapelTieraFactory agua;
	
	@Autowired
	@Qualifier("dragon")
	PiedraPapelTieraFactory dragon;
	
	@Autowired
	@Qualifier("linterna")
	PiedraPapelTieraFactory linterna;
	
	@Autowired
	@Qualifier("diablo")
	PiedraPapelTieraFactory diablo;

	@BeforeEach
	void setUp() throws Exception {

	}

	@AfterEach
	void tearDown() throws Exception {
		diablo = null;	
		dragon = null;
	}

	@Test
	void testGetInstanceDiablo() {
		assertEquals("diablo", juegoService.getIngence(PiedraPapelTieraFactory.DIABLO)
													  .getNombre()
													  .toLowerCase());
	}
	
	@Test
	void testGetInstanceDragon() {
		assertEquals("dragon", juegoService.getIngence(PiedraPapelTieraFactory.DRAGON)
													  .getNombre()
													  .toLowerCase());
	}

	//CASOS DE DIABLO 
	
	//Gana

	@Test
	void testCompararDiabloGanaAPiedra() {
		assertEquals(1, diablo.comparar(piedra));
		assertEquals("diablo le gana a piedra", diablo.getDescripcionREsultado().toLowerCase());
	}

	@Test
	void testCompararDiabloGanaATijera() {
		assertEquals(1, diablo.comparar(tijera));
		assertEquals("diablo le gana a tijera", diablo.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloGanaAFuego() {
		assertEquals(1, diablo.comparar(fuego));
		assertEquals("diablo le gana a fuego", diablo.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloGanaASerpiente() {
		assertEquals(1, diablo.comparar(serpiente));
		assertEquals("diablo le gana a serpiente", diablo.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloGanaAHumano() {
		assertEquals(1, diablo.comparar(humano));
		assertEquals("diablo le gana a humano", diablo.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloGanaAPistola() {
		assertEquals(1, diablo.comparar(pistola));
		assertEquals("diablo le gana a pistola", diablo.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloGanaALinterna() {
		assertEquals(1, diablo.comparar(linterna));
		assertEquals("diablo le gana a linterna", diablo.getDescripcionREsultado().toLowerCase());
	}
	
	
	//Pierde
	
	@Test
	void testCompararDiabloPierdeConEsponja() {
		assertEquals(-1, diablo.comparar(esponja));
		assertEquals("diablo perdi� con esponja", diablo.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloPierdeConPapel() {
		assertEquals(-1, diablo.comparar(papel));
		assertEquals("diablo perdi� con papel", diablo.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloPierdeConAire() {
		assertEquals(-1, diablo.comparar(aire));
		assertEquals("diablo perdi� con aire", diablo.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloPierdeConAgua() {
		assertEquals(-1, diablo.comparar(agua));
		assertEquals("diablo perdi� con agua", diablo.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloPierdeConDragon() {
		assertEquals(-1, diablo.comparar(dragon));
		assertEquals("diablo perdi� con dragon", diablo.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloPierdeConLobo() {
		assertEquals(-1, diablo.comparar(lobo));
		assertEquals("diablo perdi� con lobo", diablo.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloPierdeConArbol() {
		assertEquals(-1, diablo.comparar(arbol));
		assertEquals("diablo perdi� con arbol", diablo.getDescripcionREsultado().toLowerCase());
	}
	
	//Empate
	
	@Test
	void testCompararDiabloEmpataConDiablo() {
		assertEquals(0, diablo.comparar(diablo));
		assertEquals("diablo empata con diablo", diablo.getDescripcionREsultado().toLowerCase());
	}
	
	
	//CASOS DE DRAGON 
	
	//Gana
	
	@Test
	void testCompararDragonGanaAFuego() {
		assertEquals(1, dragon.comparar(fuego));
		assertEquals("dragon le gana a fuego", dragon.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonGanaATijera() {
		assertEquals(1, dragon.comparar(tijera));
		assertEquals("dragon le gana a tijera", dragon.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonGanaASerpiente() {
		assertEquals(1, dragon.comparar(serpiente));
		assertEquals("dragon le gana a serpiente", dragon.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonGanaALinterna() {
		assertEquals(1, dragon.comparar(linterna));
		assertEquals("dragon le gana a linterna", dragon.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonGanaADiablo() {
		assertEquals(1, dragon.comparar(diablo));
		assertEquals("dragon le gana a diablo", dragon.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonGanaAPistola() {
		assertEquals(1, dragon.comparar(pistola));
		assertEquals("dragon le gana a pistola", dragon.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararLinternaGanaAPiedra() {
		assertEquals(1, linterna.comparar(piedra));
		assertEquals("linterna le gana a piedra", linterna.getDescripcionREsultado().toLowerCase());
	}
	
	
	//Pierde
	
	@Test
	void testCompararDragonPierdeConEsponja() {
		assertEquals(-1, dragon.comparar(esponja));
		assertEquals("dragon perdi� con esponja", dragon.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConPapel() {
		assertEquals(-1, dragon.comparar(papel));
		assertEquals("dragon perdi� con papel", dragon.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConAire() {
		assertEquals(-1, dragon.comparar(aire));
		assertEquals("dragon perdi� con aire", dragon.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConAgua() {
		assertEquals(-1, dragon.comparar(agua));
		assertEquals("dragon perdi� con agua", dragon.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConHumano() {
		assertEquals(-1, dragon.comparar(humano));
		assertEquals("dragon perdi� con humano", dragon.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConArbol() {
		assertEquals(-1, dragon.comparar(arbol));
		assertEquals("dragon perdi� con arbol", dragon.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConLobo() {
		assertEquals(-1, dragon.comparar(lobo));
		assertEquals("dragon perdi� con lobo", dragon.getDescripcionREsultado().toLowerCase());
	}
	
	
	//Empata
	
	@Test
	void testCompararDragonEmpataConDragon() {
		assertEquals(0, dragon.comparar(dragon));
		assertEquals("dragon empata con dragon", dragon.getDescripcionREsultado().toLowerCase());
	}
}
