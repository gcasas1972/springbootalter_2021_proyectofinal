package es.edu.alten.juego.modelo.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import es.edu.alten.juego.modelo.PiedraPapelTieraFactory;
import es.edu.alten.juego.service.JuegoService;

@SpringBootTest
public class ArbolHumanoVictorTest {

	@Autowired
	JuegoService juegoService;

	@Autowired
	@Qualifier("piedra")
	PiedraPapelTieraFactory piedra;

	@Autowired
	@Qualifier("papel")
	PiedraPapelTieraFactory papel;

	@Autowired
	@Qualifier("tijera")
	PiedraPapelTieraFactory tijera;

	@Autowired
	@Qualifier("lagarto")
	PiedraPapelTieraFactory lagarto;

	@Autowired
	@Qualifier("spock")
	PiedraPapelTieraFactory spock;
		
	@Autowired
	@Qualifier("pistola")
	PiedraPapelTieraFactory pistola;
	
	@Autowired
	@Qualifier("fuego")
	PiedraPapelTieraFactory fuego;
	
	@Autowired
	@Qualifier("serpiente")
	PiedraPapelTieraFactory serpiente;
	
	@Autowired
	@Qualifier("humano")
	PiedraPapelTieraFactory humano;
	
	@Autowired
	@Qualifier("arbol")
	PiedraPapelTieraFactory arbol;
	
	@Autowired
	@Qualifier("lobo")
	PiedraPapelTieraFactory lobo;
	
	@Autowired
	@Qualifier("esponja")
	PiedraPapelTieraFactory esponja;
	
	@Autowired
	@Qualifier("aire")
	PiedraPapelTieraFactory aire;
	
	@Autowired
	@Qualifier("agua")
	PiedraPapelTieraFactory agua;
	
	@Autowired
	@Qualifier("dragon")
	PiedraPapelTieraFactory dragon;
	
	@Autowired
	@Qualifier("linterna")
	PiedraPapelTieraFactory linterna;
	
	@Autowired
	@Qualifier("diablo")
	PiedraPapelTieraFactory diablo;

	@BeforeEach
	void setUp() throws Exception {

	}

	@AfterEach
	void tearDown() throws Exception {
		arbol = null;	
		humano = null;
	}

	@Test
	void testGetInstanceDiablo() {
		assertEquals("arbol", juegoService.getIngence(PiedraPapelTieraFactory.ARBOL)
													  .getNombre()
													  .toLowerCase());
	}
	
	@Test
	void testGetInstanceDragon() {
		assertEquals("humano", juegoService.getIngence(PiedraPapelTieraFactory.HUMANO)
													  .getNombre()
													  .toLowerCase());
	}

	//CASOS DE arbol 
	
	//Gana
	
	@Test
	void testCompararDiabloGanaAPiedra() {
		assertEquals(1, arbol.comparar(diablo));
		assertEquals("arbol le gana a diablo", arbol.getDescripcionREsultado().toLowerCase());
	}

	@Test
	void testCompararDiabloGanaATijera() {
		assertEquals(1, arbol.comparar(dragon));
		assertEquals("arbol le gana a dragon", arbol.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloGanaAFuego() {
		assertEquals(1, arbol.comparar(agua));
		assertEquals("arbol le gana a agua", arbol.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloGanaASerpiente() {
		assertEquals(1, arbol.comparar(aire));
		assertEquals("arbol le gana a aire", arbol.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloGanaAHumano() {
		assertEquals(1, arbol.comparar(papel));
		assertEquals("arbol le gana a papel", arbol.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloGanaAPistola() {
		assertEquals(1, arbol.comparar(esponja));
		assertEquals("arbol le gana a esponja", arbol.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloGanaALinterna() {
		assertEquals(1, arbol.comparar(lobo));
		assertEquals("arbol le gana a lobo", arbol.getDescripcionREsultado().toLowerCase());
	}
	
	
	//Pierde
	
	@Test
	void testCompararDiabloPierdeConEsponja() {
		assertEquals(-1, arbol.comparar(linterna));
		assertEquals("arbol perdi� con linterna", arbol.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloPierdeConPapel() {
		assertEquals(-1, arbol.comparar(pistola));
		assertEquals("arbol perdi� con pistola", arbol.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloPierdeConAire() {
		assertEquals(-1, arbol.comparar(piedra));
		assertEquals("arbol perdi� con piedra", arbol.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloPierdeConAgua() {
		assertEquals(-1, arbol.comparar(fuego));
		assertEquals("arbol perdi� con fuego", arbol.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloPierdeConDragon() {
		assertEquals(-1, arbol.comparar(tijera));
		assertEquals("arbol perdi� con tijera", arbol.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloPierdeConLobo() {
		assertEquals(-1, arbol.comparar(serpiente));
		assertEquals("arbol perdi� con serpiente", arbol.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDiabloPierdeConArbol() {
		assertEquals(-1, arbol.comparar(humano));
		assertEquals("arbol perdi� con humano", arbol.getDescripcionREsultado().toLowerCase());
	}
	
	//Empate
	
	@Test
	void testCompararDiabloEmpataConDiablo() {
		assertEquals(0, arbol.comparar(arbol));
		assertEquals("arbol empata con arbol", arbol.getDescripcionREsultado().toLowerCase());
	}
	
	
	//CASOS DE humano 
	
	//Gana
	
	@Test
	void testCompararDragonGanaAFuego() {
		assertEquals(1, humano.comparar(dragon));
		assertEquals("humano le gana a dragon", humano.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonGanaATijera() {
		assertEquals(1, humano.comparar(agua));
		assertEquals("humano le gana a agua", humano.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonGanaASerpiente() {
		assertEquals(1, humano.comparar(aire));
		assertEquals("humano le gana a aire", humano.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonGanaALinterna() {
		assertEquals(1, humano.comparar(papel));
		assertEquals("humano le gana a papel", humano.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonGanaADiablo() {
		assertEquals(1, humano.comparar(esponja));
		assertEquals("humano le gana a esponja", humano.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonGanaAPistola() {
		assertEquals(1, humano.comparar(lobo));
		assertEquals("humano le gana a lobo", humano.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararLinternaGanaAPiedra() {
		assertEquals(1, humano.comparar(arbol));
		assertEquals("humano le gana a arbol", humano.getDescripcionREsultado().toLowerCase());
	}
	
	
	//Pierde
	
	@Test
	void testCompararDragonPierdeConEsponja() {
		assertEquals(-1, humano.comparar(diablo));
		assertEquals("humano perdi� con diablo", humano.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConPapel() {
		assertEquals(-1, humano.comparar(linterna));
		assertEquals("humano perdi� con linterna", humano.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConAire() {
		assertEquals(-1, humano.comparar(pistola));
		assertEquals("humano perdi� con pistola", humano.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConAgua() {
		assertEquals(-1, humano.comparar(piedra));
		assertEquals("humano perdi� con piedra", humano.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConHumano() {
		assertEquals(-1, humano.comparar(fuego));
		assertEquals("humano perdi� con fuego", humano.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConArbol() {
		assertEquals(-1, humano.comparar(tijera));
		assertEquals("humano perdi� con tijera", humano.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararDragonPierdeConLobo() {
		assertEquals(-1, humano.comparar(serpiente));
		assertEquals("humano perdi� con serpiente", humano.getDescripcionREsultado().toLowerCase());
	}
	
	
	//Empata
	
	@Test
	void testCompararDragonEmpataConDragon() {
		assertEquals(0, humano.comparar(humano));
		assertEquals("humano empata con humano", humano.getDescripcionREsultado().toLowerCase());
	}
}
