package es.edu.alten.juego.modelo.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import es.edu.alten.juego.modelo.PiedraPapelTieraFactory;
import es.edu.alten.juego.service.JuegoService;

@SpringBootTest
public class FuegoSerpienteAntonioTest {

	@Autowired
	JuegoService juegoService;

	@Autowired
	@Qualifier("piedra")
	PiedraPapelTieraFactory piedra;

	@Autowired
	@Qualifier("papel")
	PiedraPapelTieraFactory papel;

	@Autowired
	@Qualifier("tijera")
	PiedraPapelTieraFactory tijera;

	@Autowired
	@Qualifier("lagarto")
	PiedraPapelTieraFactory lagarto;

	@Autowired
	@Qualifier("spock")
	PiedraPapelTieraFactory spock;
		
	@Autowired
	@Qualifier("pistola")
	PiedraPapelTieraFactory pistola;
	
	@Autowired
	@Qualifier("fuego")
	PiedraPapelTieraFactory fuego;
	
	@Autowired
	@Qualifier("serpiente")
	PiedraPapelTieraFactory serpiente;
	
	@Autowired
	@Qualifier("humano")
	PiedraPapelTieraFactory humano;
	
	@Autowired
	@Qualifier("arbol")
	PiedraPapelTieraFactory arbol;
	
	@Autowired
	@Qualifier("lobo")
	PiedraPapelTieraFactory lobo;
	
	@Autowired
	@Qualifier("esponja")
	PiedraPapelTieraFactory esponja;
	
	@Autowired
	@Qualifier("aire")
	PiedraPapelTieraFactory aire;
	
	@Autowired
	@Qualifier("agua")
	PiedraPapelTieraFactory agua;
	
	@Autowired
	@Qualifier("dragon")
	PiedraPapelTieraFactory dragon;
	
	@Autowired
	@Qualifier("linterna")
	PiedraPapelTieraFactory linterna;
	
	@Autowired
	@Qualifier("diablo")
	PiedraPapelTieraFactory diablo;

	@BeforeEach
	void setUp() throws Exception {

	}

	@AfterEach
	void tearDown() throws Exception {
		fuego = null;	
		serpiente = null;
	}

	@Test
	void testGetInstanceFuego() {
		assertEquals("fuego", juegoService.getIngence(PiedraPapelTieraFactory.FUEGO)
													  .getNombre()
													  .toLowerCase());
	}
	
	@Test
	void testGetInstanceSerpiente() {
		assertEquals("serpiente", juegoService.getIngence(PiedraPapelTieraFactory.SERPIENTE)
													  .getNombre()
													  .toLowerCase());
	}

	//CASOS DE FUEGO 
	
	//Gana
	
	@Test
	void testCompararFuegoGanaAPapel() {
		assertEquals(1, fuego.comparar(papel));
		assertEquals("fuego le gana a papel", fuego.getDescripcionREsultado().toLowerCase());
	}

	@Test
	void testCompararFuegoGanaAEsponja() {
		assertEquals(1, fuego.comparar(esponja));
		assertEquals("fuego le gana a esponja", fuego.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararFuegoGanaALobo() {
		assertEquals(1, fuego.comparar(lobo));
		assertEquals("fuego le gana a lobo", fuego.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararFuegoGanaAArbol() {
		assertEquals(1, fuego.comparar(arbol));
		assertEquals("fuego le gana a arbol", fuego.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararFuegoGanaAHumano() {
		assertEquals(1, fuego.comparar(humano));
		assertEquals("fuego le gana a humano", fuego.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararFuegoGanaASerpiente() {
		assertEquals(1, fuego.comparar(serpiente));
		assertEquals("fuego le gana a serpiente", fuego.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararFuegoGanaATijera() {
		assertEquals(1, fuego.comparar(tijera));
		assertEquals("fuego le gana a tijera", fuego.getDescripcionREsultado().toLowerCase());
	}
	
	
	//Pierde
		
	@Test
	void testCompararFuegoPierdeConPiedra() {
		assertEquals(-1, fuego.comparar(piedra));
		assertEquals("fuego perdi� con piedra", fuego.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararFuegoPierdeConPistola() {
		assertEquals(-1, fuego.comparar(pistola));
		assertEquals("fuego perdi� con pistola", fuego.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararFuegoPierdeConLinterna() {
		assertEquals(-1, fuego.comparar(linterna));
		assertEquals("fuego perdi� con linterna", fuego.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararFuegoPierdeConDiablo() {
		assertEquals(-1, fuego.comparar(diablo));
		assertEquals("fuego perdi� con diablo", fuego.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararFuegoPierdeConDragon() {
		assertEquals(-1, fuego.comparar(dragon));
		assertEquals("fuego perdi� con dragon", fuego.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararFuegoPierdeConAgua() {
		assertEquals(-1, fuego.comparar(agua));
		assertEquals("fuego perdi� con agua", fuego.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararFuegoPierdeConAire() {
		assertEquals(-1, fuego.comparar(aire));
		assertEquals("fuego perdi� con aire", fuego.getDescripcionREsultado().toLowerCase());
	}
	
	//Empate
	
	@Test
	void testCompararFuegoEmpataConFuego() {
		assertEquals(0, fuego.comparar(fuego));
		assertEquals("fuego empata con fuego", fuego.getDescripcionREsultado().toLowerCase());
	}
	
	
	//CASOS DE SERPIENTE 
	
	//Gana
	
	@Test
	void testCompararSerpienteGanaAAgua() {
		assertEquals(1, serpiente.comparar(agua));
		assertEquals("serpiente le gana a agua", serpiente.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararSerpienteGanaAAire() {
		assertEquals(1, serpiente.comparar(aire));
		assertEquals("serpiente le gana a aire", serpiente.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararSerpienteGanaAPapel() {
		assertEquals(1, serpiente.comparar(papel));
		assertEquals("serpiente le gana a papel", serpiente.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararSerpienteGanaAEsponja() {
		assertEquals(1, serpiente.comparar(esponja));
		assertEquals("serpiente le gana a esponja", serpiente.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararSerpienteGanaALobo() {
		assertEquals(1, humano.comparar(lobo));
		assertEquals("humano le gana a lobo", humano.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararSerpienteGanaAArbol() {
		assertEquals(1, serpiente.comparar(arbol));
		assertEquals("serpiente le gana a arbol", serpiente.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararSerpienteGanaAHumano() {
		assertEquals(1, serpiente.comparar(humano));
		assertEquals("serpiente le gana a humano", serpiente.getDescripcionREsultado().toLowerCase());
	}
	
	
	//Pierde
	
	@Test
	void testCompararSerpientePierdeConTijera() {
		assertEquals(-1, serpiente.comparar(tijera));
		assertEquals("serpiente perdi� con tijera", serpiente.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararSerpientePierdeConPiedra() {
		assertEquals(-1, serpiente.comparar(piedra));
		assertEquals("serpiente perdi� con piedra", serpiente.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararSerpientePierdeConPistola() {
		assertEquals(-1, serpiente.comparar(pistola));
		assertEquals("serpiente perdi� con pistola", serpiente.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararSerpientePierdeConLinterna() {
		assertEquals(-1, serpiente.comparar(linterna));
		assertEquals("serpiente perdi� con linterna", serpiente.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararSerpientePierdeConDiablo() {
		assertEquals(-1, serpiente.comparar(diablo));
		assertEquals("serpiente perdi� con diablo", serpiente.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararSerpientePierdeConDragon() {
		assertEquals(-1, serpiente.comparar(dragon));
		assertEquals("serpiente perdi� con dragon", serpiente.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararSerpientePierdeConFuego() {
		assertEquals(-1, serpiente.comparar(fuego));
		assertEquals("serpiente perdi� con fuego", serpiente.getDescripcionREsultado().toLowerCase());
	}
	
	
	//Empata
	
	@Test
	void testCompararSerpienteEmpataConSerpiente() {
		assertEquals(0, serpiente.comparar(serpiente));
		assertEquals("serpiente empata con serpiente", serpiente.getDescripcionREsultado().toLowerCase());
	}
}
