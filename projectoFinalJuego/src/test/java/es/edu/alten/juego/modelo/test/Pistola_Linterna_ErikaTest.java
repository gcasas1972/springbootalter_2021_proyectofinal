package es.edu.alten.juego.modelo.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import es.edu.alten.juego.modelo.PiedraPapelTieraFactory;
import es.edu.alten.juego.service.JuegoService;

/**
 * Clase que testea las clases Pistola y Linterna, relizando una comparación con cada elemento
 * @author erika.castro
 *
 */
@SpringBootTest
class Pistola_Linterna_ErikaTest {

	/**
	 * Arranca el servicio
	 */
	@Autowired
	JuegoService juegoService;

	/**
	 * Atributo piedra
	 */
	@Autowired
	@Qualifier("piedra")
	PiedraPapelTieraFactory piedra;

	/**
	 * Atributo papel
	 */
	@Autowired
	@Qualifier("papel")
	PiedraPapelTieraFactory papel;

	/**
	 * Atributo tijera
	 */
	@Autowired
	@Qualifier("tijera")
	PiedraPapelTieraFactory tijera;

	/**
	 * Atributo lagarto
	 */
	@Autowired
	@Qualifier("lagarto")
	PiedraPapelTieraFactory lagarto;

	/**
	 * Atributo spock
	 */
	@Autowired
	@Qualifier("spock")
	PiedraPapelTieraFactory spock;
		
	/**
	 * Atributo pistola
	 */
	@Autowired
	@Qualifier("pistola")
	PiedraPapelTieraFactory pistola;
	
	/**
	 * Atributo fuego
	 */
	@Autowired
	@Qualifier("fuego")
	PiedraPapelTieraFactory fuego;
	
	/**
	 * Atributo serpiente
	 */
	@Autowired
	@Qualifier("serpiente")
	PiedraPapelTieraFactory serpiente;
	
	/**
	 * Atributo humano
	 */
	@Autowired
	@Qualifier("humano")
	PiedraPapelTieraFactory humano;
	
	/**
	 * Atributo arbol
	 */
	@Autowired
	@Qualifier("arbol")
	PiedraPapelTieraFactory arbol;
	
	/**
	 * Atributo lobo
	 */
	@Autowired
	@Qualifier("lobo")
	PiedraPapelTieraFactory lobo;
	
	/**
	 * Atributo esponja
	 */
	@Autowired
	@Qualifier("esponja")
	PiedraPapelTieraFactory esponja;
	
	/**
	 * Atributo aire
	 */
	@Autowired
	@Qualifier("aire")
	PiedraPapelTieraFactory aire;
	
	/**
	 * Atributo agua
	 */
	@Autowired
	@Qualifier("agua")
	PiedraPapelTieraFactory agua;
	
	/**
	 * Atributo dragon
	 */
	@Autowired
	@Qualifier("dragon")
	PiedraPapelTieraFactory dragon;
	
	/**
	 * Atributo linterna
	 */
	@Autowired
	@Qualifier("linterna")
	PiedraPapelTieraFactory linterna;
	
	/**
	 * Atributo diablo
	 */
	@Autowired
	@Qualifier("diablo")
	PiedraPapelTieraFactory diablo;

	/**
	 * Este método se ejecuta después de todo el proceso
	 * @throws Exception
	 */
	@AfterEach
	void tearDown() throws Exception {

		piedra = null;
		papel = null;
		tijera = null;
		pistola = null;	
		fuego = null;
		humano = null;
		arbol = null;
		linterna = null;
		agua = null;
		aire = null;
		dragon = null;
		diablo = null;
		esponja = null;
		serpiente = null;
		lobo = null;
	
	}

	/**
	 * Comprueba que la instancia sea la adecuada, en este caso pistola 
	 */
	@Test
	void testGetInstancePistola() {
		assertEquals("pistola", juegoService.getIngence(PiedraPapelTieraFactory.PISTOLA).getNombre().toLowerCase());

	}
	
	/**
	 * Comprueba que la instancia sea la adecuada, en este caso linterna
	 */
	@Test
	void testGetInstanceLinterna() {
		assertEquals("linterna", juegoService.getIngence(PiedraPapelTieraFactory.LINTERNA).getNombre().toLowerCase());

	}

	//CASOS DE PISTOLA 
	
	//Gana

	/**
	 * Método que compara si pistola gana a piedra
	 */
	@Test
	void testCompararPistolaGanaAPiedra() {
		assertEquals(1, pistola.comparar(piedra));
		assertEquals("pistola le gana a piedra", pistola.getDescripcionREsultado().toLowerCase());
	}

	/**
	 * Método que compara si pistola gana a tijera
	 */
	@Test
	void testCompararPistolaGanaATijera() {
		assertEquals(1, pistola.comparar(tijera));
		assertEquals("pistola le gana a tijera", pistola.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si pistola gana a fuego
	 */
	@Test
	void testCompararPistolaGanaAFuego() {
		assertEquals(1, pistola.comparar(fuego));
		assertEquals("pistola le gana a fuego", pistola.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si pistola gana a serpiente
	 */
	@Test
	void testCompararPistolaGanaASerpiente() {
		assertEquals(1, pistola.comparar(serpiente));
		assertEquals("pistola le gana a serpiente", pistola.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si pistola gana a humano
	 */
	@Test
	void testCompararPistolaGanaAHumano() {
		assertEquals(1, pistola.comparar(humano));
		assertEquals("pistola le gana a humano", pistola.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si pistola gana a arbol
	 */
	@Test
	void testCompararPistolaGanaAArbol() {
		assertEquals(1, pistola.comparar(arbol));
		assertEquals("pistola le gana a arbol", pistola.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si pistola gana a lobo
	 */
	@Test
	void testCompararPistolaGanaALobo() {
		assertEquals(1, pistola.comparar(lobo));
		assertEquals("pistola le gana a lobo", pistola.getDescripcionREsultado().toLowerCase());
	}
	
	
	//Pierde
	
	/**
	 * Método que compara si pistola pierde con esponja
	 */
	@Test
	void testCompararPistolaPierdeConEsponja() {
		assertEquals(-1, pistola.comparar(esponja));
		assertEquals("pistola pierde con esponja", pistola.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si pistola pierde con papel
	 */
	@Test
	void testCompararPistolaPierdeConPapel() {
		assertEquals(-1, pistola.comparar(papel));
		assertEquals("pistola pierde con papel", pistola.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si pistola pierde con aire
	 */
	@Test
	void testCompararPistolaPierdeConAire() {
		assertEquals(-1, pistola.comparar(aire));
		assertEquals("pistola pierde con aire", pistola.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si pistola pierde con agua
	 */
	@Test
	void testCompararPistolaPierdeConAgua() {
		assertEquals(-1, pistola.comparar(agua));
		assertEquals("pistola pierde con agua", pistola.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si pistola pierde con dragon
	 */
	@Test
	void testCompararPistolaPierdeConDragon() {
		assertEquals(-1, pistola.comparar(dragon));
		assertEquals("pistola pierde con dragon", pistola.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si pistola pierde con linterna
	 */
	@Test
	void testCompararPistolaPierdeConLinterna() {
		assertEquals(-1, pistola.comparar(linterna));
		assertEquals("pistola pierde con linterna", pistola.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si pistola pierde con diablo
	 */
	@Test
	void testCompararPistolaPierdeConDiablo() {
		assertEquals(-1, pistola.comparar(diablo));
		assertEquals("pistola pierde con diablo", pistola.getDescripcionREsultado().toLowerCase());
	}
	
	//Empate
	/**
	 * Método que compara si pistola empata consigo mismo
	 */
	@Test
	void testCompararPistolaEmpataConPistola() {
		assertEquals(0, pistola.comparar(pistola));
		assertEquals("pistola empata con pistola", pistola.getDescripcionREsultado().toLowerCase());
	}
	
	
	//CASOS DE LINTERNA 
	
	//Gana
	
	/**
	 * Método que compara si linterna gana a fuego
	 */
	@Test
	void testCompararLinternaGanaAFuego() {
		assertEquals(1, linterna.comparar(fuego));
		assertEquals("linterna le gana a fuego", linterna.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si linterna gana a tijera
	 */
	@Test
	void testCompararLinternaGanaATijera() {
		assertEquals(1, linterna.comparar(tijera));
		assertEquals("linterna le gana a tijera", linterna.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si linterna gana a serpiente
	 */
	@Test
	void testCompararLinternaGanaASerpiente() {
		assertEquals(1, linterna.comparar(serpiente));
		assertEquals("linterna le gana a serpiente", linterna.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si linterna gana a humano
	 */
	@Test
	void testCompararLinternaGanaAHumano() {
		assertEquals(1, linterna.comparar(humano));
		assertEquals("linterna le gana a humano", linterna.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si linterna gana a arbol
	 */
	@Test
	void testCompararLinternaGanaAArbol() {
		assertEquals(1, linterna.comparar(arbol));
		assertEquals("linterna le gana a arbol", linterna.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si linterna gana a pistola
	 */
	@Test
	void testCompararLinternaGanaAPistola() {
		assertEquals(1, linterna.comparar(pistola));
		assertEquals("linterna le gana a pistola", linterna.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si linterna gana a piedra
	 */
	@Test
	void testCompararLinternaGanaAPiedra() {
		assertEquals(1, linterna.comparar(piedra));
		assertEquals("linterna le gana a piedra", linterna.getDescripcionREsultado().toLowerCase());
	}
	
	
	//Pierde
	
	/**
	 * Método que compara si linterna pierde con esponja
	 */
	@Test
	void testCompararLinternaPierdeConEsponja() {
		assertEquals(-1, linterna.comparar(esponja));
		assertEquals("linterna pierde con esponja", linterna.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si linterna pierde con papel
	 */
	@Test
	void testCompararLinternaPierdeConPapel() {
		assertEquals(-1, linterna.comparar(papel));
		assertEquals("linterna pierde con papel", linterna.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si linterna pierde con aire
	 */
	@Test
	void testCompararLinternaPierdeConAire() {
		assertEquals(-1, linterna.comparar(aire));
		assertEquals("linterna pierde con aire", linterna.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si linterna pierde con agua
	 */
	@Test
	void testCompararLinternaPierdeConAgua() {
		assertEquals(-1, linterna.comparar(agua));
		assertEquals("linterna pierde con agua", linterna.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si linterna pierde con dragon
	 */
	@Test
	void testCompararLinternaPierdeConDragon() {
		assertEquals(-1, linterna.comparar(dragon));
		assertEquals("linterna pierde con dragon", linterna.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si linterna pierde con diablo
	 */
	@Test
	void testCompararLinternaPierdeConDiablo() {
		assertEquals(-1, linterna.comparar(diablo));
		assertEquals("linterna pierde con diablo", linterna.getDescripcionREsultado().toLowerCase());
	}
	
	/**
	 * Método que compara si linterna pierde con lobo
	 */
	@Test
	void testCompararLinternaPierdeConLobo() {
		assertEquals(-1, linterna.comparar(lobo));
		assertEquals("linterna pierde con lobo", linterna.getDescripcionREsultado().toLowerCase());
	}
	
	
	//Empata
	
	/**
	 * Método que compara si linterna empata consigo mismo 
	 */
	@Test
	void testCompararLinternaEmpataConLinterna() {
		assertEquals(0, linterna.comparar(linterna));
		assertEquals("linterna empata con linterna", linterna.getDescripcionREsultado().toLowerCase());
	}
	
}
