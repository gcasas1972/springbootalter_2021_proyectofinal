package es.edu.alten.juego.modelo.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import es.edu.alten.juego.modelo.PiedraPapelTieraFactory;
import es.edu.alten.juego.service.JuegoService;

@SpringBootTest
public class AireAguaIvanTest {

	@Autowired
	JuegoService juegoService;

	@Autowired
	@Qualifier("piedra")
	PiedraPapelTieraFactory piedra;

	@Autowired
	@Qualifier("papel")
	PiedraPapelTieraFactory papel;

	@Autowired
	@Qualifier("tijera")
	PiedraPapelTieraFactory tijera;

	@Autowired
	@Qualifier("lagarto")
	PiedraPapelTieraFactory lagarto;

	@Autowired
	@Qualifier("spock")
	PiedraPapelTieraFactory spock;
		
	@Autowired
	@Qualifier("pistola")
	PiedraPapelTieraFactory pistola;
	
	@Autowired
	@Qualifier("fuego")
	PiedraPapelTieraFactory fuego;
	
	@Autowired
	@Qualifier("serpiente")
	PiedraPapelTieraFactory serpiente;
	
	@Autowired
	@Qualifier("humano")
	PiedraPapelTieraFactory humano;
	
	@Autowired
	@Qualifier("arbol")
	PiedraPapelTieraFactory arbol;
	
	@Autowired
	@Qualifier("lobo")
	PiedraPapelTieraFactory lobo;
	
	@Autowired
	@Qualifier("esponja")
	PiedraPapelTieraFactory esponja;
	
	@Autowired
	@Qualifier("aire")
	PiedraPapelTieraFactory aire;
	
	@Autowired
	@Qualifier("agua")
	PiedraPapelTieraFactory agua;
	
	@Autowired
	@Qualifier("dragon")
	PiedraPapelTieraFactory dragon;
	
	@Autowired
	@Qualifier("linterna")
	PiedraPapelTieraFactory linterna;
	
	@Autowired
	@Qualifier("diablo")
	PiedraPapelTieraFactory diablo;

	@BeforeEach
	void setUp() throws Exception {

	}

	@AfterEach
	void tearDown() throws Exception {
		agua = null;	
		aire = null;
	}

	@Test
	void testGetInstanceAire() {
		assertEquals("aire", juegoService.getIngence(PiedraPapelTieraFactory.AIRE)
													  .getNombre()
													  .toLowerCase());
	}
	
	@Test
	void testGetInstanceAgua() {
		assertEquals("agua", juegoService.getIngence(PiedraPapelTieraFactory.AGUA)
													  .getNombre()
													  .toLowerCase());
	}

	//CASOS DE AGUA
	
	//Gana

	@Test
	void testCompararAguaGanaAFuego() {
		assertEquals(1, agua.comparar(fuego));
		assertEquals("agua le gana a fuego", agua.getDescripcionREsultado().toLowerCase());
	}

	@Test
	void testCompararAguaGanaAPiedra() {
		assertEquals(1, agua.comparar(piedra));
		assertEquals("agua le gana a piedra", agua.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAguaGanaAPistola() {
		assertEquals(1, agua.comparar(pistola));
		assertEquals("agua le gana a pistola", agua.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAguaGanaALinterna() {
		assertEquals(1, agua.comparar(linterna));
		assertEquals("agua le gana a linterna", agua.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAguaGanaADiablo() {
		assertEquals(1, agua.comparar(diablo));
		assertEquals("agua le gana a diablo", agua.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAguaGanaADragon() {
		assertEquals(1, agua.comparar(dragon));
		assertEquals("agua le gana a dragon", agua.getDescripcionREsultado().toLowerCase());
	}
	

	@Test
	void testCompararAguaGanaATijera() {
		assertEquals(1, agua.comparar(tijera));
		assertEquals("agua le gana a tijera", agua.getDescripcionREsultado().toLowerCase());
	
	}
	//Pierde
	
	@Test
	void testCompararAguaPierdeConAire() {
		assertEquals(-1, agua.comparar(aire));
		assertEquals("agua perdi� con aire", agua.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAguaPierdeConPapel() {
		assertEquals(-1, agua.comparar(papel));
		assertEquals("agua perdi� con papel", agua.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAguaPierdeConEsponja() {
		assertEquals(-1, agua.comparar(esponja));
		assertEquals("agua perdi� con esponja", agua.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAguaPierdeConLobo() {
		assertEquals(-1, agua.comparar(lobo));
		assertEquals("agua perdi� con lobo", agua.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAguaPierdeConArbol() {
		assertEquals(-1, agua.comparar(arbol));
		assertEquals("agua perdi� con arbol", agua.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAguaPierdeConHumano() {
		assertEquals(-1, agua.comparar(humano));
		assertEquals("agua perdi� con humano", agua.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAguaPierdeConSerpiente() {
		assertEquals(-1, agua.comparar(serpiente));
		assertEquals("agua perdi� con serpiente", agua.getDescripcionREsultado().toLowerCase());
	}
	
	//Empate
	
	@Test
	void testCompararAguaEmpataConAire() {
		assertEquals(0, agua.comparar(agua));
		assertEquals("agua empata con agua", agua.getDescripcionREsultado().toLowerCase());
	}
	
	
	//CASOS DE AIRE
	
	//Gana

	@Test
	void testCompararAireGanaAFuego() {
		assertEquals(1, aire.comparar(fuego));
		assertEquals("aire le gana a fuego", aire.getDescripcionREsultado().toLowerCase());
	}

	@Test
	void testCompararAireGanaAPiedra() {
		assertEquals(1, aire.comparar(piedra));
		assertEquals("aire le gana a piedra", aire.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAireGanaAPistola() {
		assertEquals(1, aire.comparar(pistola));
		assertEquals("aire le gana a pistola", aire.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAireGanaALinterna() {
		assertEquals(1, aire.comparar(linterna));
		assertEquals("aire le gana a linterna", aire.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAireGanaADiablo() {
		assertEquals(1, aire.comparar(diablo));
		assertEquals("aire le gana a diablo", aire.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAireGanaADragon() {
		assertEquals(1, aire.comparar(dragon));
		assertEquals("aire le gana a dragon", aire.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAireGanaAAgua() {
		assertEquals(1, aire.comparar(agua));
		assertEquals("aire le gana a agua", aire.getDescripcionREsultado().toLowerCase());
	}
	
	
	
	//Pierde
	
	@Test
	void testCompararAirePierdeConSerpiente() {
		assertEquals(-1, aire.comparar(serpiente));
		assertEquals("aire perdi� con serpiente", aire.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAirePierdeConPapel() {
		assertEquals(-1, aire.comparar(papel));
		assertEquals("aire perdi� con papel", aire.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAirePierdeConEsponja() {
		assertEquals(-1, aire.comparar(esponja));
		assertEquals("aire perdi� con esponja", aire.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAirePierdeConLobo() {
		assertEquals(-1, aire.comparar(lobo));
		assertEquals("aire perdi� con lobo", aire.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAirePierdeConArbol() {
		assertEquals(-1, aire.comparar(arbol));
		assertEquals("aire perdi� con arbol", aire.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAirePierdeConHumano() {
		assertEquals(-1, aire.comparar(humano));
		assertEquals("aire perdi� con humano", aire.getDescripcionREsultado().toLowerCase());
	}
	
	@Test
	void testCompararAirePierdeConTijera() {
		assertEquals(-1, aire.comparar(tijera));
		assertEquals("aire perdi� con tijera", aire.getDescripcionREsultado().toLowerCase());
	}
	
	//Empate
	
	@Test
	void testCompararAireEmpataConAire() {
		assertEquals(0, aire.comparar(aire));
		assertEquals("aire empata con aire", aire.getDescripcionREsultado().toLowerCase());
	}
	
}
