package es.edu.alten.juego.modelo.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import es.edu.alten.juego.modelo.PiedraPapelTieraFactory;
import es.edu.alten.juego.service.JuegoService;

@SpringBootTest
class EsponjaLoboYeliTest {
	
	@Autowired
	JuegoService juegoService;
	
	@Autowired
	@Qualifier("piedra")
	PiedraPapelTieraFactory piedra;

	@Autowired
	@Qualifier("papel")
	PiedraPapelTieraFactory papel;

	@Autowired
	@Qualifier("tijera")
	PiedraPapelTieraFactory tijera;
		
	@Autowired
	@Qualifier("pistola")
	PiedraPapelTieraFactory pistola;
	
	@Autowired
	@Qualifier("linterna")
	PiedraPapelTieraFactory linterna;
	
	@Autowired
	@Qualifier("diablo")
	PiedraPapelTieraFactory diablo;
	
	@Autowired
	@Qualifier("dragon")
	PiedraPapelTieraFactory dragon;
	
	@Autowired
	@Qualifier("agua")
	PiedraPapelTieraFactory agua;
	
	@Autowired
	@Qualifier("aire")
	PiedraPapelTieraFactory aire;
	
	@Autowired
	@Qualifier("esponja")
	PiedraPapelTieraFactory esponja;
	
	@Autowired
	@Qualifier("lobo")
	PiedraPapelTieraFactory lobo;
	
	@Autowired
	@Qualifier("arbol")
	PiedraPapelTieraFactory arbol;
	
	@Autowired
	@Qualifier("humano")
	PiedraPapelTieraFactory humano;
	
	@Autowired
	@Qualifier("serpiente")
	PiedraPapelTieraFactory serpiente;
	
	@Autowired
	@Qualifier("fuego")
	PiedraPapelTieraFactory fuego;

	@AfterEach
	void tearDown() throws Exception {
	
		esponja   = 	null;
		lobo      = 	null;
	
	}
	
	@Test
	void testGetInstancePistola() {
		assertEquals("esponja", juegoService.getIngence(PiedraPapelTieraFactory.ESPONJA)
														.getNombre()
														.toLowerCase());
	}
	
	@Test
	void testGetInstanceLinterna() {
		assertEquals("lobo", juegoService.getIngence(PiedraPapelTieraFactory.LOBO)
													.getNombre()
													.toLowerCase());
	}

	//================================================================================
    // Casos de ESPONJA
    //================================================================================
	
	//*************** GANA ***************//
	@Test
	void testCompararEsponjaGanaAPistola() {
		assertEquals(1, esponja.comparar(pistola));
		assertEquals("esponja le gana a pistola", esponja.getDescripcionREsultado()
														 .toLowerCase());
	}
	@Test
	void testCompararEsponjaGanaALinterna() {
		assertEquals(1, esponja.comparar(linterna));
		assertEquals("esponja le gana a linterna", esponja.getDescripcionREsultado()
														  .toLowerCase());
	}
	@Test
	void testCompararEsponjaGanaADiablo() {
		assertEquals(1, esponja.comparar(diablo));
		assertEquals("esponja le gana a diablo", esponja.getDescripcionREsultado()
														.toLowerCase());
	}
	@Test
	void testCompararEsponjaGanaADragon() {
		assertEquals(1, esponja.comparar(dragon));
		assertEquals("esponja le gana a dragon", esponja.getDescripcionREsultado()
														 .toLowerCase());
	}
	@Test
	void testCompararEsponjaGanaAAgua() {
		assertEquals(1, esponja.comparar(agua));
		assertEquals("esponja le gana a agua", esponja.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	void testCompararEsponjaGanaAAire() {
		assertEquals(1, esponja.comparar(aire));
		assertEquals("esponja le gana a aire", esponja.getDescripcionREsultado()
														 .toLowerCase());
	}
	@Test
	void testCompararEsponjaGanaAPapel() {
		assertEquals(1, esponja.comparar(papel));
		assertEquals("esponja le gana a papel", esponja.getDescripcionREsultado()
													   .toLowerCase());
	}
	
	//*************** PIERDE ***************//
	
	@Test
	void testCompararEsponjaPierdeConPiedra() {
		assertEquals(-1, esponja.comparar(piedra));
		assertEquals("esponja perdió con piedra", esponja.getDescripcionREsultado()
													      .toLowerCase());
	}
	@Test
	void testCompararEsponjaPierdeConFuego() {
		assertEquals(-1, esponja.comparar(fuego));
		assertEquals("esponja perdió con fuego", esponja.getDescripcionREsultado()
													     .toLowerCase());
	}
	@Test
	void testCompararEsponjaPierdeConTijera() {
		assertEquals(-1, esponja.comparar(tijera));
		assertEquals("esponja perdió con tijera", esponja.getDescripcionREsultado()
													      .toLowerCase());
	}
	@Test
	void testCompararEsponjaPierdeConSerpiente() {
		assertEquals(-1, esponja.comparar(serpiente));
		assertEquals("esponja perdió con serpiente", esponja.getDescripcionREsultado()
													         .toLowerCase());
	}
	@Test
	void testCompararEsponjaPierdeConHumano() {
		assertEquals(-1, esponja.comparar(humano));
		assertEquals("esponja perdió con humano", esponja.getDescripcionREsultado()
													     .toLowerCase());
	}
	@Test
	void testCompararEsponjaPierdeConArbol() {
		assertEquals(-1, esponja.comparar(arbol));
		assertEquals("esponja perdió con arbol", esponja.getDescripcionREsultado()
													     .toLowerCase());
	}
	@Test
	void testCompararEsponjaPierdeConLobo() {
		assertEquals(-1, esponja.comparar(lobo));
		assertEquals("esponja perdió con lobo", esponja.getDescripcionREsultado()
													    .toLowerCase());
	}	
	
	//*************** EMPATA ***************//
	
	@Test
	void testCompararEsponjaEmpataConEsponja() {
		assertEquals(0, esponja.comparar(esponja));
		assertEquals("esponja empata con esponja", esponja.getDescripcionREsultado()
													      .toLowerCase());
	}
	
	//================================================================================
    // Casos de LOBO
    //================================================================================
	
	//*************** GANA ***************//
	
	@Test
	void testCompararLoboGanaConLinterna() {
		//TODO para mis queridos alumnos testCompararPapelGanaConPiedra
		assertEquals(1, lobo.comparar(linterna));
		assertEquals("lobo le gana a linterna", lobo.getDescripcionREsultado()
													.toLowerCase());
	}
	@Test
	void testCompararLoboGanaConDiablo() {
		//TODO para mis queridos alumnos testCompararPapelGanaConPiedra
		assertEquals(1, lobo.comparar(diablo));
		assertEquals("lobo le gana a diablo", lobo.getDescripcionREsultado()
												  .toLowerCase());
	}
	@Test
	void testCompararLoboGanaConDragon() {
		//TODO para mis queridos alumnos testCompararPapelGanaConPiedra
		assertEquals(1, lobo.comparar(dragon));
		assertEquals("lobo le gana a dragon", lobo.getDescripcionREsultado()
												  .toLowerCase());
	}
	@Test
	void testCompararLoboGanaConAgua() {
		//TODO para mis queridos alumnos testCompararPapelGanaConPiedra
		assertEquals(1, lobo.comparar(agua));
		assertEquals("lobo le gana a agua", lobo.getDescripcionREsultado()
												.toLowerCase());
	}
	@Test
	void testCompararLoboGanaConAire() {
		//TODO para mis queridos alumnos testCompararPapelGanaConPiedra
		assertEquals(1, lobo.comparar(aire));
		assertEquals("lobo le gana a aire", lobo.getDescripcionREsultado()
												.toLowerCase());
	}
	@Test
	void testCompararLoboGanaConPapel() {
		//TODO para mis queridos alumnos testCompararPapelGanaConPiedra
		assertEquals(1, lobo.comparar(papel));
		assertEquals("lobo le gana a papel", lobo.getDescripcionREsultado()
												 .toLowerCase());
	}
	@Test
	void testCompararLoboGanaConEsponja() {
		//TODO para mis queridos alumnos testCompararPapelGanaConPiedra
		assertEquals(1, lobo.comparar(esponja));
		assertEquals("lobo le gana a esponja", lobo.getDescripcionREsultado()
													.toLowerCase());
	}
	
	//*************** PIERDE ***************//
	
	@Test
	void testCompararLoboPierdeConPistola() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, lobo.comparar(pistola));
		assertEquals("lobo perdió con pistola", lobo.getDescripcionREsultado()
												    .toLowerCase());
	}
	@Test
	void testCompararLoboPierdeConPiedra() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, lobo.comparar(piedra));
		assertEquals("lobo perdió con piedra", lobo.getDescripcionREsultado()
												   .toLowerCase());
	}
	@Test
	void testCompararLoboPierdeConFuego() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, lobo.comparar(fuego));
		assertEquals("lobo perdió con fuego", lobo.getDescripcionREsultado()
												  .toLowerCase());
	}
	@Test
	void testCompararLoboPierdeConTijera() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, lobo.comparar(tijera));
		assertEquals("lobo perdió con tijera", lobo.getDescripcionREsultado()
												   .toLowerCase());
	}
	@Test
	void testCompararLoboPierdeConSerpiente() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, lobo.comparar(serpiente));
		assertEquals("lobo perdió con serpiente", lobo.getDescripcionREsultado()
													  .toLowerCase());
	}
	@Test
	void testCompararLoboPierdeConHumano() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, lobo.comparar(humano));
		assertEquals("lobo perdió con humano", lobo.getDescripcionREsultado()
												   .toLowerCase());
	}
	@Test
	void testCompararLoboPierdeConArbol() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, lobo.comparar(arbol));
		assertEquals("lobo perdió con arbol", lobo.getDescripcionREsultado()
												  .toLowerCase());
	}
	
	//*************** EMPATA ***************//
	@Test
	void testCompararLoboaEmpataConLobo() {
		assertEquals(0, lobo.comparar(lobo));
		assertEquals("lobo empata con lobo", lobo.getDescripcionREsultado()
												 .toLowerCase());
	}
	

}
